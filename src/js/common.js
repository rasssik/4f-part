// fixed svg show
//-----------------------------------------------------------------------------

svg4everybody();

$(document).ready(function () {


	// about us slider
	$('.js-about-us-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		arrows: false,
		fade: true,
		autoplay: true,
		autoplaySpeed: 6000,
		asNavFor: '.js-about-us-slider-navigation',

	});
	$('.js-about-us-slider-navigation').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		asNavFor: '.js-about-us-slider',
		dots: false,
		centerMode: true,
		focusOnSelect: true,
		mobileFirst: true,

		responsive: [{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 3,
					infinite: true,
					dots: true
				}
			},
			{
				breakpoint: 1024,
				settings: {
					slidesToShow: 3,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 400,
				settings: {
					slidesToShow: 2,
					slidesToScroll: 1
				}
			},
			{
				breakpoint: 330,
				settings: {
					slidesToShow: 1,
					slidesToScroll: 1
				}
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		]
	});

	$("#general-slider").vegas({
		preload: true,
		/*load then show image*/
		autoplay: true,
		loop: true,
		shuffle: false,
		cover: true,
		transition: 'fade',
		timer: false,
		// prev: '#arrow_left',
		// next: '#arrow_right',
		transitionDuration: 10000,
		delay: 10000,
		/*slide duration*/
		// overlay: 'vegas/overlays/01.png' /*overlay background*/

		slides: [{
				src: "img/general-slider-1.jpg"
			},
			{
				src: "img/general-slider-3.jpg"
			},
			{
				src: "img/general-slider-2.jpg"
			},
			{
				src: "img/general-slider-4.jpg"
			}
		]


	});
	$('#next').on('click', function () {
		$("#general-slider").vegas('next');
	});
	$('#previous').on('click', function () {
		$("#general-slider").vegas('previous');
	});
	$('a#next').on('click', function () {
		$elmt.vegas('options', 'transition', 'slideLeft2').vegas('next');
	});
	$(function () {
		$('.js-language-btn').click(function () {
			var active = $(this).parents('.js-language').find('.active')
			if (active.length) {
				active.removeClass('active');
			}
			$(this).addClass('active');
		});
	});
	$(".js-link-to").on('click', function () {
		var elementClick = $(this).attr("href")
		var destination = $(elementClick).offset().top - 20;
		$("html:not(:animated),body:not(:animated)").animate({
			scrollTop: destination
		}, 800);
		return false;
	});
	$(".b-dropdown").on("click", function (e) {
		if ($(this).hasClass("is-open")) {
			$(this).removeClass("is-open");
			$(this).children("ul").slideUp("fast");
		} else {
			$(this).addClass("is-open");
			$(this).children("ul").slideDown("fast");
		}
	});
	$('.btn-close-menu').click(function () {
		$('.header').removeClass('is-open');
		$(this).closest("body").removeClass('js-block-scroll');

	});
	$('.btn-menu').click(function () {
		$('.header').addClass('is-open');
		$("body").addClass('js-block-scroll');

	});


	$('.js-title-cut').ellipsis({
		lines: 3,
		ellipClass: 'ellip',
		responsive: true
	});
	$('.js-title-cut-third').ellipsis({
		lines: 2,
		ellipClass: 'ellip',
		responsive: true
	});



	$(".dropdown-menu").click(function () {
		$(this).toggleClass('is-open');
	});
	$('.dropdown').dropdown();

	$('.js-general-reviews-slider').slick({
		slidesToShow: 1,
		slidesToScroll: 1,
		autoplay: false,
		autoplaySpeed: 2000,
		dots: true,
		arrows: false,

	});


	$('.js-open-popup').magnificPopup({
		type: 'inline',
		preloader: false
	});
	// pageWidget
	function pageWidget(pages) {
		var widgetWrap = $('<div class="widget_wrap"><div class="widget_inner"><ul class="widget_list"></ul></div></div>');
		widgetWrap.prependTo("body");
		for (var i = 0; i < pages.length; i++) {
			if (pages[i][0] === '#') {
				$('<li class="widget_item"><a class="widget_link" href="' + pages[i] + '">' + pages[i] + '</a></li>').appendTo('.widget_list');
			} else {
				$('<li class="widget_item"><a class="widget_link" href="' + pages[i] + '.html' + '">' + pages[i] + '</a></li>').appendTo('.widget_list');
			}
		}
		var widgetStilization = $('<style>body {position:relative} .widget_list {max-height:700px} .widget_inner{overflow-y: auto;}.widget_wrap{position:fixed;top:0;left:0;z-index:9999;box-shadow: 0 2px 35px rgba(8, 13, 45, 0.14);padding:20px 20px 10px;background:#273083;border-bottom-right-radius:5px;-webkit-transition:all .3s ease;transition:all .3s ease;-webkit-transform:translate(-100%,0);-ms-transform:translate(-100%,0);transform:translate(-100%,0)}.widget_wrap:after{content:" ";position:absolute;top:0;left:100%;width:24px;height:24px;background:#273083 url(data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAAQAgMAAABinRfyAAAABGdBTUEAALGPC/xhBQAAAAxQTFRF////////AAAA////BQBkwgAAAAN0Uk5TxMMAjAd+zwAAACNJREFUCNdjqP///y/DfyBg+LVq1Xoo8W8/CkFYAmwA0Kg/AFcANT5fe7l4AAAAAElFTkSuQmCC) no-repeat 50% 50%;pointer-events: all;cursor:pointer}.widget_wrap:hover{-webkit-transform:translate(0,0);-ms-transform:translate(0,0);transform:translate(0,0)}.widget_item{padding:0 0 3px}.widget_link{color:#fff;text-decoration:none;font-size:12px;font-weight:500;}.widget_link:hover{text-decoration:underline} </style>');
		widgetStilization.prependTo(".widget_wrap");
	};

	$(document).ready(function ($) {
		pageWidget(['index', 'single-news', 'news', 'order-form', 'contacts', 'vip', 'about-us', 'reviews']);
	});
	// pageWidget End
});


$(document).ready(function () {
	$('.js-catalog-slider').slick({
		// centerPadding: '60px',
		slidesToShow: 1,
		mobileFirst: true,
		arrows: false,
		centerMode: true,

		// centerMode: true,
		variableWidth: true,
		responsive: [{
				breakpoint: 575,
				settings: "unslick",

			},
			{
				breakpoint: 480,
				settings: {
					arrows: false,
					centerMode: true,
					centerPadding: '40px',
					slidesToShow: 1
				}
			}
		]
	});
	// $('.js-slider').slick({
	// 	dots: false,
	// 	infinite: true,
	// 	arrows: true,
	// 	slidesToShow: 1,
	// 	slidesToScroll: 1,
	// 	autoplay: false,
	// 	mobileFirst: true
	// });
	$('.js-slider').slick({
		slidesToShow: 1,
		arrows: false,
		centerMode: true,
		variableWidth: true,
		mobileFirst: true,
		responsive: [{
				breakpoint: 1024,
				settings: {

					infinite: true,
				}
			},
			{
				breakpoint: 768,
				settings: {
					arrows: true,
					centerMode: false,
					variableWidth: false,


				}
			},
			{
				breakpoint: 480,
				settings: {

				}
			}
			// You can unslick at a given breakpoint now by adding:
			// settings: "unslick"
			// instead of a settings object
		]
	});
	// $('.js-slider').slick({
	// 	// centerPadding: '60px',
	// 	slidesToShow: 1,
	// 	mobileFirst: true,
	// 	arrows: true,
	// 	centerMode: true,

	// 	// centerMode: true,
	// 	variableWidth: true,
	// 	responsive: [{
	// 			breakpoint: 575,
	// 			arrows: false

	// 		},
	// 		{
	// 			breakpoint: 480,
	// 			settings: {
	// 				centerMode: true,
	// 				centerPadding: '40px',
	// 				slidesToShow: 1,
	// 				arrows: false

	// 			}
	// 		}
	// 	]
	// });
	$('.js-date').bootstrapMaterialDatePicker({
		time: false,
		clearButton: true,
		currentDate: null,
		weekStart: 1,
		minDate: new Date()
	});


	$('.js-time').bootstrapMaterialDatePicker({
		date: false,
		shortTime: false,
		format: 'HH:mm'

	});
});

$(window).scroll(function () {
	if ($(this).scrollTop() > 0) {
		$('#to-top').fadeIn().css("display", "flex");
	} else {
		$('#to-top').fadeOut();
	}
});
$(function () {
	$(".phone").mask("+9(999) 999-9999");
});
$('#to-top').click(function () {
	$('body,html').animate({
		scrollTop: 0
	}, 400);
	return false;
});