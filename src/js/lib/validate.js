// // custom jQuery validation
// // add to validate form class 'js-validate'
// // add to fields attr 'name'
// //-----------------------------------------------------------------------------------
// var validator = {
// 	init: function () {
// 		$('form').each(function () {
// 			var $form = $(this);
// 			var config = {
// 				errorElement: 'b',
// 				errorClass: 'is-error',
// 				validClass: 'is-valid',
// 				focusInvalid: false,
// 				focusCleanup: true,
// 				errorPlacement: function (error, element) {
// 					validator.setError($(element), error);
// 				},
// 				highlight: function (element, errorClass, validClass) {
// 					var $el = validator.defineElement($(element));
// 					var $elWrap = $el.closest('.form-group');

// 					if ($el) $el.removeClass(validClass).addClass(errorClass);
// 					if ($elWrap.length) $elWrap.removeClass(validClass).addClass(errorClass);
// 				},
// 				unhighlight: function (element, errorClass, validClass) {
// 					var $el = validator.defineElement($(element));
// 					var $elWrap = $el.closest('.form-group');

// 					if ($el) $el.removeClass(errorClass).addClass(validClass);
// 					if ($elWrap.length) $elWrap.removeClass(errorClass).addClass(validClass);
// 				}
// 			};
// 			if ($form.hasClass('js-validate')) {
// 				$form.validate(config);
// 			}
// 		});
// 	},
// 	setError: function ($el, message) {
// 		$el = this.defineElement($el);
// 		if ($el) this.domWorker.error($el, message);
// 	},
// 	defineElement: function ($el) {
// 		return $el;
// 	},
// 	domWorker: {
// 		error: function ($el, message) {
// 			var $elWrap = $el.closest('.form-group');
// 			$el.addClass('is-error');
// 			if ($elWrap.length) $elWrap.addClass('is-error');
// 			$el.after(message);
// 		}
// 	}
// };

// validator.init();

// // validate by data attribute
// //-----------------------------------------------------------------------------------
// (function () {
// 	// add to validate field data-valid="test"
// 	//-----------------------------------------------------------------------------------
// 	var rules = {
// 		'surname': {
// 			required: true,
// 			minlength: 2,
// 			messages: {
// 				required: "Not a valid surname",
// 				minlength: 'Minimum length 2 characters'
// 			}
// 		},
// 		'phone': {
// 			required: true,
// 			digits: true,
// 			minlength: 9,
// 			messages: {
// 				required: "Not a valid phone",
// 				digits: 'Please enter only digits',
// 				minlength: 'Minimum length 9 characters'
// 			}
// 		},
// 		'name': {
// 			required: true,
// 			minlength: 2,
// 			messages: {
// 				required: "Not a valid name",
// 				minlength: 'Minimum length 2 characters'
// 			}
// 		},

// 		'message': {
// 			required: true,
// 			minlength: 10,
// 			messages: {
// 				required: "Not valid comment",
// 				minlength: 'Not valid comment'
// 			}
// 		},
// 		'email': {
// 			required: true,
// 			email: true,
// 			messages: {
// 				required: "Not a valid e-mail",
// 				email: 'Not a valid e-mail'
// 			}
// 		},
// 		'password': {
// 			required: true,
// 			minlength: 6,
// 			maxlength: 16,
// 			messages: {
// 				required: "This field is required.",
// 				minlength: 'Must have at least 6 characters!',
// 				maxlength: 'Maximum 16 characters'
// 			}
// 		},
// 		'confirm-password': {
// 			required: true,
// 			minlength: 6,
// 			equalTo: "#password",
// 			messages: {
// 				required: "You didn't confirm the password",
// 				minlength: 'Must have at least 6 characters!',
// 				equalTo: 'Passwords must match'
// 			}
// 		},
// 		'account-password': {
// 			minlength: 6,
// 			messages: {
// 				minlength: 'Must have at least 6 characters!'
// 			}
// 		},
// 		'account-confirm-password': {

// 			minlength: 6,
// 			equalTo: "#password",
// 			messages: {
// 				minlength: 'Must have at least 6 characters!',
// 				equalTo: 'Passwords must match'
// 			}
// 		},
// 		'password-req': {
// 			required: false,
// 			minlength: 6,
// 			messages: {
// 				required: "This field is required.",
// 				minlength: 'Must have at least 6 characters!'
// 			}
// 		},
// 		'confirm-password-req': {
// 			required: false,
// 			minlength: 6,
// 			equalTo: "#password",
// 			messages: {
// 				required: "You didn't confirm the password",
// 				minlength: 'Must have at least 6 characters!',
// 				equalTo: 'Passwords must match'
// 			}
// 		}
// 	};


// 	for (var ruleName in rules) {
// 		$('[data-valid=' + ruleName + ']').each(function () {
// 			$(this).rules('add', rules[ruleName]);
// 		});
// 	};
// }());

// // global messages
// //-----------------------------------------------------------------------------------
// $.validator.messages.minlength = 'At least {0} characters';

// // custom rules
// //-----------------------------------------------------------------------------------
// $.validator.addMethod("email", function (value) {
// 	if (value == '') return true;
// 	var regexp = /[a-zA-Z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*@(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?/;
// 	return regexp.test(value);
// });

// $.validator.addMethod("letters", function (value, element) {
// 	return this.optional(element) || /^[^1-9!@#\$%\^&\*\(\)\[\]:;,.?=+_<>`~\\\/"]+$/i.test(value);
// });

// $.validator.addMethod("digits", function (value, element) {
// 	return this.optional(element) || /^(\+?\d+)?\s*(\(\d+\))?[\s-]*([\d-]*)$/i.test(value);
// });

// $.validator.addMethod('dataValidError', function (value, element) {
// 	var $el = validator.defineElement($(element));

// 	return this.optional(element) || !$el.attr('data-valid-error');
// });

$().ready(function () {

	// validate signup form on keyup and submit
	$(".js-validate").validate({
		highlight: function(element) {
			$(element).parent().addClass("is-error");
	},
	unhighlight: function(element) {
			$(element).parent().removeClass("is-error");
	},
		errorClass: 'is-error',
		errorElement: "b",
		rules: {
			firstname: {
				required: true,
				minlength: 2
			},
			lastname: {
				required: true,
				minlength: 2
			},
			username: {
				required: true,
				minlength: 2
			},
			select: {
				required: true
			},
			password: {
				required: true,
				minlength: 5
			},
			confirm_password: {
				required: true,
				minlength: 5,
				equalTo: "#password"
			},
			email: {
				required: true,
				email: true
			},
			topic: {
				required: "#newsletter:checked",
				minlength: 2
			},
			agree: "required"
		},
		messages: {
			firstname: "Please enter your name",
			lastname: "Please enter your surname",
			username: {
				required: "Please enter a username",
				minlength: "Your username must consist of at least 2 characters"
			},
			password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long"
			},
			confirm_password: {
				required: "Please provide a password",
				minlength: "Your password must be at least 5 characters long",
				equalTo: "Please enter the same password as above"
			},
			email: "Please enter a valid email address",
			agree: "Please accept our policy",
			topic: "Please select at least 2 topics"
		}
	});
});

$('#menu-list').dropdown({
	onChange: function() {
		$('#return').removeAttr('checked');

	}
 });
 